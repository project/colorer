<?php
global $colorer_include_presets, $colorer_exclude_presets;

$colorer_include_presets = array(
	'all' => array('all', 'textarea'),
	'node' => array('node', '#node-form textarea'),
	'body' => array('node body', '#node-form #edit-body'),
	'block' => array('block body', '#block-box-form textarea'),
	'site' => array('site information', '#system-site-information-settings textarea'),
	'comment' => array('comments', '#comment-form #edit-comment'),
);

$colorer_exclude_presets = array(
	'log' => array('log message', '#edit-log'),
);
