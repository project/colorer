
function colorer_editarea(id, textarea) {
	language = colorer_language;
	if (language == 'zh-hans') language = 'zh';
	textarea.after('<style type=text/css>.grippie {display:none;}</style>');
	editAreaLoader.init({
		id: id,
		syntax: 'php',
		start_highlight: true,
		language: language,
		allow_toggle: false,
		word_wrap: true,
		allow_resize: 'y',
		toolbar: "search, fullscreen, |, undo, redo, |, select_font,|, word_wrap, |, help"
	});
}

function colorer_codemirror(id, textarea) {
	var editor = CodeMirror.fromTextArea(
		id, {
        parserfile: [
			'parsexml.js',
			'parsecss.js',
			'tokenizejavascript.js',
			'parsejavascript.js',
            '../contrib/php/js/tokenizephp.js',
			'../contrib/php/js/parsephp.js',
            '../contrib/php/js/parsephphtmlmixed.js'],
        stylesheet: [
			colorer_url + '/codemirror/css/xmlcolors.css',
			colorer_url + '/codemirror/css/jscolors.css',
			colorer_url + '/codemirror/css/csscolors.css',
			colorer_url + '/codemirror/contrib/php/css/phpcolors.css'
		],
        path: colorer_url + '/codemirror/js/',
        continuousScanning: 500,
		tabMode: 'shift'
	});
	$('.CodeMirror-wrapping iframe').css('border', '1px solid #666');
}

$(document).ready(function() {

	if (colorer_callback && colorer_include && colorer_url) {
		textareas = $(colorer_include);
		if (colorer_exclude) textareas = textareas.not(colorer_exclude);
		textareas.each(function() {
			textarea = $(this);
			id = textarea.attr('id');
			textarea.removeClass('editor-textarea');
			if (id != '') eval(colorer_callback + '(id, textarea);');
		});
	}

});
