<?php

include_once('colorer.presets.inc');

// settings

function _colorer_include() {
	global $colorer_include_presets;
	$include = preg_split("/\r?\n/", trim(variable_get('colorer_include_other', '')));
	if (count($include) == 1 && $include[0] == '') $include = array();
	$options = variable_get('colorer_include', array('node' => 'node', 'block' => 'block'));
	if ($options) {
		foreach ($options as $option => $yes) {
			$selector = $colorer_include_presets[$option];
			if ($yes && $selector) {
				$include[] = $selector[1];
			}
		}
	}
	return join(", ", $include);
}

function _colorer_exclude() {
	global $colorer_exclude_presets;
	$exclude = preg_split("/\r?\n/", trim(variable_get('colorer_exclude_other', '')));
	if (count($exclude) == 1 && $exclude[0] == '') $exclude = array();
	$options = variable_get('colorer_exclude', array('log' => 'log'));
	if ($options) {
		foreach ($options as $option => $yes) {
			$selector = $colorer_exclude_presets[$option];
			if ($yes && $selector) {
				$exclude[] = $selector[1];
			}
		}
	}
	return join(", ", $exclude);
}

function _colorer_engine() {
	return variable_get('colorer_engine', 'editarea');
}

// hooks

function colorer_perm() {
	return array('use colorer', 'administer colorer', 'switch colorer');
}

function colorer_form_alter(&$form, &$form_state, $form_id) {
	static $count = 0;
	global $user;
	if (!user_access('use colorer') || user_access('switch colorer') && isset($user->colorer_switch) && !$user->colorer_switch || $count) return;
	$count++;

	$module_path = drupal_get_path('module', 'colorer');
	$module_url  = url($module_path);
	$language = $GLOBALS['language']->language;

	if (_colorer_engine() == 'codemirror') {
		drupal_add_js("$module_path/codemirror/js/codemirror.js");
		$callback = 'colorer_codemirror';
	} else {
		drupal_add_js("$module_path/editarea/edit_area/edit_area_full.js");
		$lang_file = $language == 'zh-hans' ? 'zh' : $language;
		if (!file_exists("$module_path/editarea/edit_area/langs/${lang_file}.js")) $language = 'en';
		$callback = 'colorer_editarea';
	}
	drupal_add_js("$module_path/script.js");
	drupal_add_js("var colorer_include='" . _colorer_include() . "';\n" .
	              "var colorer_exclude='" . _colorer_exclude() . "';\n" .
	              "var colorer_url=\"$module_url\";\n" .
	              "var colorer_callback=\"$callback\";\n" .
	              "var colorer_language=\"$language\";", 'inline');
}

function colorer_user($op, &$edit, &$account, $category = NULL) {
	if ($op == 'form' && $category == 'account' && user_access('switch colorer')) {
		$form['colorer'] = array(
			'#type' => 'fieldset',
			'#title' => t('Colorer'),
			'#weight' => 10,
		);
		$form['colorer']['colorer_switch'] = array(
			'#type' => 'checkbox',
			'#title' => t('Switch colorer'),
			'#default_value' => isset($edit['colorer_switch']) ? $edit['colorer_switch'] : 1,
		);
		return $form;
	}
}

function colorer_menu() {
	$items['admin/settings/colorer'] = array(
		'title' => 'Colorer',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('colorer_settings'),
		'access arguments' => array('administer colorer')
	);
	return $items;
}

function colorer_settings() {
	global $colorer_include_presets, $colorer_exclude_presets;
	$form['colorer_engine'] = array(
		'#type' => 'radios',
		'#title' => 'Engine',
		'#default_value' => _colorer_engine(),
		'#options' => array('editarea' => 'EditArea', 'codemirror' => 'CodeMirror'),
	);

	$options = array();
	foreach ($colorer_include_presets as $option => $value) {
		$options[$option] = $value[0];
	}
	$form['colorer_include'] = array(
		'#type' => 'checkboxes',
		'#title' => 'Include',
		'#options' => $options,
		'#default_value' => variable_get('colorer_include', array('node' => 'node', 'block' => 'block')),
	);
	$form['colorer_include_other'] = array(
		'#type' => 'textarea',
		'#description' => 'Selectors one per line.',
		'#default_value' => variable_get('colorer_include_other', ''),
	);

	$options = array();
	foreach ($colorer_exclude_presets as $option => $value) {
		$options[$option] = $value[0];
	}
	$form['colorer_exclude'] = array(
		'#type' => 'checkboxes',
		'#title' => 'Exclude',
		'#options' => $options,
		'#default_value' => variable_get('colorer_exclude', array('log' => 'log')),
	);
	$form['colorer_exclude_other'] = array(
		'#type' => 'textarea',
		'#description' => 'Selectors one per line.',
		'#default_value' => variable_get('colorer_exclude_other', ''),
	);

	return system_settings_form($form);
}
